package context

import (
	"gitee.com/Alimjan2009/golang_wechat/v2/openplatform/config"
)

// Context struct
type Context struct {
	*config.Config
}
