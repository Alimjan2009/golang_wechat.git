package context

import (
	"gitee.com/Alimjan2009/golang_wechat/v2/credential"
	"gitee.com/Alimjan2009/golang_wechat/v2/miniprogram/config"
)

// Context struct
type Context struct {
	*config.Config
	credential.AccessTokenHandle
}
